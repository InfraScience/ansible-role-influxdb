Role Name
=========

A role that install Influxdb, <https://www.influxdata.com> 

Role Variables
--------------

The most important variables are listed below:

``` yaml
influxdb_deb_repo: "deb https://repos.influxdata.com/ubuntu bionic stable"
influxdb_repo_key: 'https://repos.influxdata.com/influxdb.key'
influxdb_pkgs:
  - influxdb
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
